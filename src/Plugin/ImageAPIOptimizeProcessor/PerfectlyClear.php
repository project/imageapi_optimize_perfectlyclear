<?php

namespace Drupal\imageapi_optimize_perfectlyclear\Plugin\ImageAPIOptimizeProcessor;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Image\ImageFactory;
use Drupal\imageapi_optimize\ConfigurableImageAPIOptimizeProcessorBase;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Uses the PerfectlyClear.io webservice to optimize an image.
 *
 * @ImageAPIOptimizeProcessor(
 *   id = "perfectlyclear",
 *   label = @Translation("PerfectlyClear.io"),
 *   description = @Translation("Uses the PerfectlyClear.io service to optimize images.")
 * )
 */
final class PerfectlyClear extends ConfigurableImageAPIOptimizeProcessorBase {

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger, ImageFactory $image_factory, FileSystemInterface $file_system, ClientInterface $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger, $image_factory);

    $this->fileSystem = $file_system;
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('imageapi_optimize'),
      $container->get('image.factory'),
      $container->get('file_system'),
      $container->get('http_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function applyToImage($image_uri) {
    // Only process JPG images.
    if ($this->getMimeType($image_uri) !== 'image/jpeg') {
      return FALSE;
    }

    // Get PerfectlyClear.io API key from configuration.
    $apiKey = $this->configuration['api_key'];

    // Need to send the file off to the remote service and await a response.
    try {
      $sourceImage = fopen($image_uri, 'r');

      $response = $this->httpClient->put('https://api.perfectlyclear.io/v1/pfc', [
        'headers' => [
          'X-API-KEY' => $apiKey,
          'Content-Type' => 'image/jpeg'
        ],
        'body' => $sourceImage
      ]);

      $body = $response->getBody();
      $json = json_decode($body);

      // If this has worked, we should get a dest entry in the JSON returned.
      if (!empty($json->imageKey)) {
        try {
          $response = $this->httpClient->get('https://api.perfectlyclear.io/v1/pfc/' . $json->imageKey, [
            'headers' => [
              'X-API-KEY' => $apiKey
            ]
          ]);
          $body = $response->getBody();
          $json = json_decode($body);

          if (!empty($json->corrected_url)) {
            // Now go fetch that, and save it locally.
            $optimizedFile = $this->httpClient->get($json->corrected_url);
            if ($optimizedFile->getStatusCode() == 200) {
              if ($this->fileSystem->saveData($optimizedFile->getBody(), $image_uri, FileSystemInterface::EXISTS_REPLACE)) {
                return TRUE;
              }
            }
          }
        }
        catch (RequestException $e) {
          $this->logger->error('Failed to download optimize image using perfectlyclear.io due to "%error".', ['%error' => $e->getMessage()]);
        }

      }
    }
    catch (RequestException $e) {
      $this->logger->error('Failed to download optimize image using perfectlyclear.io due to "%error".', ['%error' => $e->getMessage()]);
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api_key' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('PerfectlyClear API key'),
      '#description' => $this->t('Enter required PerfectlyClear.io API key. Get your API key from <a href="https://perfectlyclear.io" target="_blank">https://perfectlyclear.io</a>'),
      '#default_value' => $this->configuration['api_key'],
      '#size' => 32,
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->configuration['api_key'] = $form_state->getValue('api_key');
  }

}
